#!/bin/bash -eu

TMPFILE=$(mktemp)

. ./server.conf

###############################################################################

# Create new targets
curl \
    --basic -u admin:admin \
    'http://'$SERVER'/rest/v1/targets' \
    -i \
    -X POST \
    -H 'Content-Type: application/json;charset=UTF-8' \
    -d '[ {
        "controllerId" : "1",
        "name" : "BeagleBone Black",
        "description" : "This is a test target"
    } ]' \
| tee $TMPFILE
echo ""
echo ""
cat $TMPFILE | tail -1 | jq .

###############################################################################

rm -f $TMPFILE
