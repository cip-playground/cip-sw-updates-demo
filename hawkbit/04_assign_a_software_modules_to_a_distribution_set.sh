#!/bin/bash -eu

TMPFILE=$(mktemp)

. ./server.conf

###############################################################################

assign_a_software_modules_to_a_distribution_set()
{
    curl \
        --basic -u admin:admin \
        'http://'$SERVER'/rest/v1/distributionsets/'$1'/assignedSM' \
        -i \
        -X POST \
        -H 'Content-Type: application/json;charset=UTF-8' \
        -d '[ {
            "id" : '$1'
        } ]' \
        2>&1 \
    | tee $TMPFILE
    echo ""
    echo ""
    cat $TMPFILE | tail -1 | jq .
}

for i in $(seq 1 7); do
    assign_a_software_modules_to_a_distribution_set $i
done

###############################################################################

rm -f $TMPFILE
