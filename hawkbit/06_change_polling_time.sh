#!/bin/bash -eu

TMPFILE=$(mktemp)

. ./server.conf

###############################################################################

change_polling_time()
{
    # 30sec is a minimum time supported by hawkBit
    curl \
        --basic -u admin:admin \
        'http://'$SERVER'/rest/v1/system/configs/pollingTime' \
        -i \
        -X PUT \
        -H 'Content-Type: application/json' \
        -d '{
            "value" : "00:00:30"
        }' \
        2>&1 \
    | tee $TMPFILE
    echo ""
    echo ""
    cat $TMPFILE | tail -1 | jq .
}

change_polling_time

###############################################################################

rm -f $TMPFILE
