#!/bin/bash -eu

VERSION=$1

TMPFILE=$(mktemp)

. ./server.conf
. ./swu.conf

###############################################################################

create_new_software_module()
{
    curl \
        --basic -u admin:admin \
        'http://'$SERVER'/rest/v1/softwaremodules' \
        -i \
        -X POST \
        -H 'Content-Type: application/hal+json;charset=UTF-8' \
        -d '[ {
            "vendor" : "CIP",
            "name" : "'"$1"'",
            "description" : "This is a test software module",
            "version" : "'$2'",
            "type" : "os"
        } ]' \
        2>&1 \
    | tee $TMPFILE
    echo ""
    echo ""
    cat $TMPFILE | tail -1 | jq .
}

i=0
while true; do
    i=$(($i + 1))
    MOD_NAME=$(echo $MOD_NAME_AND_VERSION_LIST | cut -d',' -f$i)
    i=$(($i + 1))
    MOD_VERSION=$(echo $MOD_NAME_AND_VERSION_LIST | cut -d',' -f$i)
    if [ -z "$MOD_NAME" ]; then
        break
    else
        create_new_software_module "$MOD_NAME" "$MOD_VERSION"
    fi
done

###############################################################################

rm -f $TMPFILE
