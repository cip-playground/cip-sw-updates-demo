#!/bin/bash -eu

. ./server.conf

TMPFILE=$(mktemp)

###############################################################################

# Enable the checkbox of "Allow a gateway to authenticate and manage multiple targets through a gateway security token"
curl \
    --basic -u admin:admin \
    'http://'$SERVER'/rest/v1/system/configs/authentication.gatewaytoken.enabled' \
    -i \
    -X PUT \
    -H 'Content-Type: application/json' \
    -d '{
        "value" : true
    }' \
| tee $TMPFILE
echo ""
echo ""
cat $TMPFILE | tail -1 | jq .

# Add a gateway token
curl \
    --basic -u admin:admin \
    'http://'$SERVER'/rest/v1/system/configs/authentication.gatewaytoken.key' \
    -i \
    -X PUT \
    -H 'Content-Type: application/json' \
    -d '{
        "value" : "exampleToken"
    }' \
| tee $TMPFILE
echo ""
echo ""
cat $TMPFILE | tail -1 | jq .

###############################################################################

rm -f $TMPFILE
