#!/bin/bash -eu

VERSION=$1

TMPFILE=$(mktemp)

. ./server.conf
. ./swu.conf

###############################################################################

make_new_distribution_sets()
{
    curl \
        --basic -u admin:admin \
        'http://'$SERVER'/rest/v1/distributionsets' \
        -i \
        -X POST \
        -H 'Content-Type: application/json;charset=UTF-8' \
        -d '[ {
            "name" : "'"$1"'",
            "description" : "This is a test target",
            "version" : "'$2'",
            "type" : "os"
        } ]' \
        2>&1 \
    | tee $TMPFILE
    echo ""
    echo ""
    cat $TMPFILE | tail -1 | jq .
}

i=0
while true; do
    i=$(($i + 1))
    DIST_NAME=$(echo $DIST_NAME_AND_VERSION_LIST | cut -d',' -f$i)
    i=$(($i + 1))
    DIST_VERSION=$(echo $DIST_NAME_AND_VERSION_LIST | cut -d',' -f$i)
    if [ -z "$DIST_NAME" ]; then
        break
    else
        make_new_distribution_sets "$DIST_NAME" "$DIST_VERSION"
    fi
done

###############################################################################

rm -f $TMPFILE
