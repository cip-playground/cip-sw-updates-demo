#!/bin/bash -eu

# 1 is enable
if [ $1 -eq 1 ]; then
    sed -i 's%^\( \+\)\#\(true\)%\1\2%' $2
    sed -i 's%^\( \+\)\(false\)%\1#\2%' $2
# 0 is disable
elif [ $1 -eq 0 ]; then
    sed -i 's%^\( \+\)\(true\)%\1#\2%' $2
    sed -i 's%^\( \+\)#\(false\)%\1\2%' $2
else
    exit 1
fi
