#!/bin/bash -eu

sed -i ":lbl1;N;s/\(-- u-boot configuration\nlocal configuration = \[\[\n\[bootloader\]\nname=uboot\n\n\[roundrobin\]\nchainhandler=\)\(raw\|rdiff_image\)/\1$1/;b lbl1;" $2
