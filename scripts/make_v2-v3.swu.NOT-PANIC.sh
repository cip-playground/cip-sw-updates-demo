#!/usr/bin/env bash

set -eu

PRODUCT_NAME="bbb-image"
FILES="sw-description v2-v3.delta"

cp sw-description.rdiff.v2-v3 sw-description

for i in $FILES;do
	echo $i;done | cpio -ov -H crc >  ${PRODUCT_NAME}.v2-v3.NOT-PANIC.swu

rm sw-description
