#!/usr/bin/env bash

set -eu

PRODUCT_NAME="bbb-image"
FILES="sw-description v2-v1.delta"

cp sw-description.rdiff.v2-v1 sw-description

for i in $FILES;do
	echo $i;done | cpio -ov -H crc >  ${PRODUCT_NAME}.v2-v1.INIT.swu

rm sw-description
