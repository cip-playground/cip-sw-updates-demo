#!/bin/bash -eu

# 1 is enable
if [ $1 -eq 1 ]; then
    sed -i 's%^\t#\(# to deliverately cause a kernel panic\)%\t\1%' $2
    sed -i 's%^\t#\(install -v -m 0755 ${WORKDIR}/sysrq-panic.sh ${D}/root/\)%\t\1%' $2
    sed -i 's%^\t#\(install -v -d ${D}/lib/systemd/system\)%\t\1%' $2
    sed -i 's%^\t#\(install -v -m 0644 ${WORKDIR}/sysrq-panic.service ${D}/lib/systemd/system/\)%\t\1%' $2
    sed -i 's%^\t#\(install -v -d ${D}/etc/systemd/system/default.target.wants\)%\t\1%' $2
    sed -i 's%^\t#\(ln -s /lib/systemd/system/sysrq-panic.service ${D}/etc/systemd/system/default.target.wants/\)%\t\1%' $2
# 0 is disable
elif [ $1 -eq 0 ]; then
    sed -i 's%^\t\(# to deliverately cause a kernel panic\)%\t#\1%' $2
    sed -i 's%^\t\(install -v -m 0755 ${WORKDIR}/sysrq-panic.sh ${D}/root/\)%\t#\1%' $2
    sed -i 's%^\t\(install -v -d ${D}/lib/systemd/system\)%\t#\1%' $2
    sed -i 's%^\t\(install -v -m 0644 ${WORKDIR}/sysrq-panic.service ${D}/lib/systemd/system/\)%\t#\1%' $2
    sed -i 's%^\t\(install -v -d ${D}/etc/systemd/system/default.target.wants\)%\t#\1%' $2
    sed -i 's%^\t\(ln -s /lib/systemd/system/sysrq-panic.service ${D}/etc/systemd/system/default.target.wants/\)%\t#\1%' $2
else
    exit 1
fi
