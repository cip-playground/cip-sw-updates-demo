# CIP SW Updates WG demo

## Prerequisites

* The following packages are required:
    * for building demo images
        * docker-ce
        * rdiff
    * for installing v1 image to a microSD
        * bmap-tools
    * for running hawkBit
        * docker-ce
    * for uploading update images to hawkBit
        * jq
* Required free space for built images
    * ossj2019: at least 12 GiB
* Required time for building (Intel(R) Core(TM) i7-2600 CPU @ 3.40GHz (4 cores, 8 threads))
    * ossj2019: about 1.5 hours

## How to use

### Make a microSD card

```
$ ./build.sh [--avoid-badproxy] ossj2019
```
* If your build server is under a proxy server and "Hash Sum mismatch" error occurs, try to use "--avoid-badproxy" option.

### Delete built images to crean this repository

```
$ ./clean.sh
```

### Demo procedure

1. Initialization (Execute only once before starting a demo day)
    1. Start hawkBit on a laptop
        ```
        $ docker start <docker container name>
        ```
        * If you don't have docker container of hawkBit, you can pull and run it as follows:
            ```
            $ pushd hawkbit
            $ ./run_hawkbit.sh
            $ popd
            ```
            * NOTE: run_hawkbit.sh use 8083 port by default. If you want to change it, please edit the script.
    1. Access http://localhost:8083 via web browser and login by admin:admin
    1. Execute the following script to do an initial setting and upload update images
        ```
        $ pushd hawkbit
        $ ./run_only_once_before_starting_a_demo_day.sh ossj2019
        $ popd
        ```
    1. Insert a microSD to a card reader and execute the following script to write an initial image (v1 image)
        ```
        $ <Before executing install.sh, make sure the device files of a microSD and write it to TARGET_DEV in wic/bmap_wic_and_dd_ext4.sh>
        $ ./install.sh
        ```
    1. Connect a serial console via screen
        ```
        $ sudo screen /dev/ttyUSB0 115200
        ```
    1. Insert microSD to BBB and boot it with keeping holding down S2 button
1. On hawkBit dashboard, Drag & drop the following update images in "Distributions" block to "BeagleBone Black" in "Targets" block and click "OK". After that, wait for downloading the update image from BBB at least 30 seconds.
    1. For OSSJ 2019
        1. [1] v1-v2.raw
            1. If you want to update soon, execute the following command with a short polling interval on BBB:
                ```
                # hawkbitcfg <polling interval (seconds)>
                ```
        1. [2] v2-v3.rdiff
            * NOTE: Current binary delta update is not correct because it needs to synchronize both partitions before updating
        1. [3] v3-v4.raw.PANIC
        1. [4] v3-v4.raw.TEST-FAILURE
        1. [5] v3-v4.raw
            1. During writing the update image to a partition, power it off on purpose
                * NOTE: Current u-boot environment doesn't have redundancy, so this test might break the environment and BBB won't boot. You shouldn't do this demo for now.
        1. Initialization (Return BBB's version to v1)
            1. If current version is v2, drag & drop "[INIT] v2-v1.rdiff" to the target
            1. If current version is v3 or v4, drag & drop "[INIT] v3v4-v1.raw" to the target
1. Initialization (Delete all update history)
    1. Execute the following script to re-create target to delete all update history
        ```
        $ pushd hawkbit
        $ ./run_after_a_demo_end.sh
        $ popd
        ```

### Important notice

* Screen updates of hawkBit dashboard somtimes fails. If so, you have to update the screen manually.
