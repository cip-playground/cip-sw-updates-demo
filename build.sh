#!/bin/bash -eu

###############################################################################
# functions
###############################################################################

usage()
{
    echo "$0 [--avoid-badproxy] ossj2019"
}

build()
{
    pushd build/isar-cip-core
    ./kas-docker --isar build kas.yml:board-bbb.yml
    popd
}

make_ext4_image()
{
    # comment out recipes-core/customizations/customizations.bb's last paragraph (enable kernel panic)
    scripts/enable_kernel_panic.sh $1 build/isar-cip-core/recipes-core/customizations/customizations.bb

    # change test to true at recipes-core/customizations/files/swupdate.sh
    scripts/enable_test_true.sh $2 build/isar-cip-core/recipes-core/customizations/files/swupdate.sh

    # change chainhandler to $1 at recipes-core/swupdate/files/swupdate_handlers.lua
    scripts/change_chainhandler.sh $3 build/isar-cip-core/recipes-core/swupdate/files/swupdate_handlers.lua

    # change recipes-core/customizations/files/rootfs_version to $2
    scripts/change_rootfs_ver.sh $4 build/isar-cip-core/recipes-core/customizations/files/rootfs_version

    # delete build/tmp/stamps/swupdate-2019.04+-git+isar-r0.do_*
    rm -rf build/isar-cip-core/build/tmp/stamps/swupdate-2019.04+-git+isar-r0.do_*

    # delete build/tmp/stamps/customizations-1.0-r0.do_*
    rm -rf build/isar-cip-core/build/tmp/stamps/customizations-1.0-r0.do_*

    # delete build/tmp/stamps/cip-core-image-1.0-r0.do_ext4_image.*
    rm -rf build/isar-cip-core/build/tmp/stamps/cip-core-image-1.0-r0.do_ext4_image.*

    # build
    build
}

make_ext4_image_and_copy()
{
    make_ext4_image $1 $2 $3 $4

    # copy ext4.img to swu directory and rename it to add ".v2.NOT-PANIC" suffix
    mv build/isar-cip-core/build/tmp/deploy/images/bbb/cip-core-image-cip-core-bbb.ext4.img swu
    mv swu/cip-core-image-cip-core-bbb.ext4.img swu/cip-core-image-cip-core-bbb.ext4.img.$4.$5
}

make_wic_and_bmap()
{
    # change IMAGE_TYPE to ext4-img at conf/machine/bbb.conf
    scripts/enable_wic_img_build.sh 0 build/isar-cip-core/conf/machine/bbb.conf

    # delete build/tmp/stamps/cip-core-image-1.0-r0.do_*
    rm -rf build/isar-cip-core/build/tmp/stamps/cip-core-image-1.0-r0.do_*

    make_ext4_image 0 1 raw v1

    # apply wic bmap patch to isar
    pushd build/isar-cip-core/isar
    patch -p1 < ../../../scripts/add_wic_bmap.patch
    popd

    # change IMAGE_TYPE to wic-img at conf/machine/bbb.conf
    scripts/enable_wic_img_build.sh 1 build/isar-cip-core/conf/machine/bbb.conf

    # delete build/tmp/stamps/cip-core-image-1.0-r0.do_*
    rm -rf build/isar-cip-core/build/tmp/stamps/cip-core-image-1.0-r0.do_*

    # build
    build

    # copy wic.img and wic.img.bmap to wic directory
    mkdir -p wic
    cp build/isar-cip-core/build/tmp/deploy/images/bbb/cip-core-image-cip-core-bbb.wic.img* wic

    ## Make v1 (NOT-PANIC) ext4

    # change IMAGE_TYPE to ext4-img at conf/machine/bbb.conf
    scripts/enable_wic_img_build.sh 0 build/isar-cip-core/conf/machine/bbb.conf

    # build
    build

    # copy ext4.img to swu directory and rename it to add ".v1.NOT-PANIC" suffix
    mkdir -p swu
    mv build/isar-cip-core/build/tmp/deploy/images/bbb/cip-core-image-cip-core-bbb.ext4.img swu
    mv swu/cip-core-image-cip-core-bbb.ext4.img swu/cip-core-image-cip-core-bbb.ext4.img.v1.NOT-PANIC
}

make_swu()
{
    # Make v1-v2 (-> NOT-PANIC) raw swu
    cp scripts/make_$1.sh swu
    pushd swu
    ./make_$1.sh
    popd
}

###############################################################################
# main
###############################################################################

if [ $# -eq 1 ]; then
    OPT=
    VERSION=$1
elif [ $# -eq 2 ]; then
    OPT=$1
    VERSION=$2
    if [ $OPT != "--avoid-badproxy" ]; then
        echo "\"$OPT\" is not supported. We support only \"--avoid-badproxy\"."
        exit 1
    fi
else
    usage
    exit 1
fi

if [ $VERSION != "ossj2019" ]; then
    echo "\"$VERSION\" is not supported. We support only \"ossj2019\"."
    exit 1
fi

mkdir -p build

pushd build
if [ $VERSION = "ossj2019" ]; then
    git clone https://gitlab.com/cip-project/cip-core/isar-cip-core.git
    cd isar-cip-core
    git checkout stormc/swupdate
    git reset --hard 7ac709f07b568d8c6057fa0f4745f5341a963409
fi
wget https://raw.githubusercontent.com/siemens/kas/master/kas-docker
chmod a+x kas-docker
popd

if [ "$OPT" = "--avoid-badproxy" ]; then
    pushd build/isar-cip-core
    git clone -q https://github.com/ilbers/isar.git
    cd isar
    git checkout -q 596732aa99c361b756655434bc90e0108e1caa33
    cp ../../../scripts/99fixbadproxy meta/recipes-devtools/buildchroot/files
    cp ../../../scripts/99fixbadproxy meta/recipes-core/isar-bootstrap/files
    patch -p1 < ../../../scripts/add_bad_proxy_mesure_to_buildroot.patch
    patch -p1 < ../../../scripts/add_bad_proxy_mesure_to_isar-bootstrap.patch
    popd
fi

## Fix wrong changes

# for customization
sed -i '/^\t\(# temporary solution\)/d' build/isar-cip-core/recipes-core/customizations/customizations.bb
sed -i '/^\t\(ln -sf \/boot\/vmlinuz-\* ${D}\/boot\/vmlinuz\)/d' build/isar-cip-core/recipes-core/customizations/customizations.bb
sed -i '/^\t\(ln -sf \/boot\/initrd-\* ${D}\/boot\/initrd\)/d' build/isar-cip-core/recipes-core/customizations/customizations.bb

# for u-boot
sed -i 's%/boot/%%g' build/isar-cip-core/recipes-bsp/u-boot/files/boot-bbb.scr.in
sed -i 's%0:${bootpart_root}%0:1%g' build/isar-cip-core/recipes-bsp/u-boot/files/boot-bbb.scr.in

# for cip-core-image
sed -i 's%^    #\(# add /boot to /etc/fstab for fw_printenv\)%    \1%' build/isar-cip-core/recipes-core/images/cip-core-image.bb
sed -i 's%^    #\(echo "/dev/mmcblk0p1 /boot vfat defaults,nofail 0 0" | sudo tee -a ${IMAGE_ROOTFS}/etc/fstab\)%    \1%' build/isar-cip-core/recipes-core/images/cip-core-image.bb

if [ $VERSION = "ossj2019" ]; then
    ## Make several images

    # Make v1 (NOT-PANIC) wic and bmap
    make_wic_and_bmap

    # Make ext4 images ($1: enable kernel panic, $2: have test true)
    make_ext4_image_and_copy 0 1 rdiff_image v2 NOT-PANIC    # Make v2 (NOT-PANIC) ext4 (chainhandler=rdiff_image)
    make_ext4_image_and_copy 0 1 raw v3 NOT-PANIC            # Make v3 (NOT-PANIC) ext4
    make_ext4_image_and_copy 1 1 raw v4 PANIC                # Make v4 (PANIC) ext4
    make_ext4_image_and_copy 0 0 raw v4 TEST-FAILURE         # Make v4 (test failure) ext4
    make_ext4_image_and_copy 0 1 raw v4 NOT-PANIC            # Make v4 (NOT-PANIC) ext4

    ## Make delta related files

    # Make v2 (NOT-PANIC) sig
    cp scripts/make_v2.sig.sh swu
    pushd swu
    ./make_v2.sig.sh
    popd

    # Make v2-v3 (-> NOT-PANIC) delta
    cp scripts/make_v2-v3.delta.sh swu
    pushd swu
    ./make_v2-v3.delta.sh
    popd

    # Make v2-v1 (-> Initialize) delta
    cp scripts/make_v2-v1.delta.sh swu
    pushd swu
    ./make_v2-v1.delta.sh
    popd

    ## Make swus

    # Copy sw-description files
    cp scripts/sw-description.* swu

    # Make swu
    make_swu v1-v2.swu.NOT-PANIC    # Make v1-v2 (-> NOT-PANIC) raw swu
    make_swu v2-v3.swu.NOT-PANIC    # Make v2-v3 (-> NOT-PANIC) delta swu
    make_swu v3-v4.swu.PANIC        # Make v3-v4 (-> PANIC) raw swu
    make_swu v3-v4.swu.TEST-FAILURE # Make v3-v4 (-> test failure) raw swu
    make_swu v3-v4.swu.NOT-PANIC    # Make v3-v4 (-> NOT-PANIC) raw swu
    make_swu v2-v1.swu.INIT         # Make v2-v1 (-> Initialize) delta swu
    make_swu v3v4-v1.swu.INIT       # Make v3v4-v1 (-> Initialize) raw swu
else
    echo "$VERSION is not supported."
    exit 1
fi

## Copy swu to the hawkbit directory
cp swu/bbb-image.* hawkbit

## Copy v1 NOT-PANIC ext4 image to wic directory
cp swu/cip-core-image-cip-core-bbb.ext4.img.v1.NOT-PANIC wic

## Copy a burning script to wic directory
cp scripts/bmap_wic_and_dd_ext4.sh wic
